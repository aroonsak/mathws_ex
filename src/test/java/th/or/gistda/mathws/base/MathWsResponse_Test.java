package th.or.gistda.mathws.base;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.junit.Test;

public class MathWsResponse_Test {

	private String createExpectedJSONString(String json) {
		
		JSONObject jObj = new JSONObject(json);
		return jObj.toString();
	}
	
	@Test
	public void testToHttpBody_Http() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		res.setHttp(200, "Description for 200");
		
		String expectedJson =
				  "{"
				+ "    \"http\": {"
				+ "        status:200,"
				+ "        description:\"Description for 200\""
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
	
	@Test
	public void testToHttpBody_Service() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		res.setService("Service_wow", "Exciting_operation");
		
		String expectedJson =
				  "{"
				+ "    \"service\": {"
				+ "        name:\"Service_wow\","
				+ "        operation:\"Exciting_operation\""
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
	
	@Test
	public void testToHttpBody_HttpAndService() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		res.setHttp(400, "This should not happen.");
		res.setService("Service_123", "456_operation");
		
		String expectedJson =
				  "{"
				+ "    \"service\": {"
				+ "        name:\"Service_123\","
				+ "        operation:\"456_operation\""
				+ "    },"
				+ "    \"http\": {"
				+ "        status: 400,"
				+ "        description: \"This should not happen.\""
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
	
	@Test
	public void testToHttpBody_Input() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		List<Double> inputA = new ArrayList<Double>() {{
			add(100.111);
			add(200.222);
			add(300.333);
			add(400.444);
			add(500.555);
		}};
		List<Double> inputB = new ArrayList<Double>() {{
			add(999.999);
			add(888.888);
			add(777.777);
			add(666.666);
			add(555.555);
		}};
		res.setInput(inputA, inputB);
		
		String expectedJson =
				  "{"
				+ "    \"input\": {"
				+ "        a: [100.111, 200.222, 300.333, 400.444, 500.555],"
				+ "        b: [999.999, 888.888, 777.777, 666.666, 555.555]"
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
	
	@Test
	public void testToHttpBody_Output() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		List<Double> outputC = new ArrayList<Double>() {{
			add(100.111);
			add(200.222);
			add(300.333);
			add(400.444);
			add(500.555);
		}};
		res.setOutput(outputC);
		
		String expectedJson =
				  "{"
				+ "    \"output\": {"
				+ "        c: [100.111, 200.222, 300.333, 400.444, 500.555],"
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
	
	@Test
	public void testToHttpBody_InputOutput() {
		
		//Prepare
		IMathWsResponse res = new MathWsResponse();
		List<Double> inputA = new ArrayList<Double>() {{
			add(100.111);
			add(200.222);
			add(300.333);
			add(400.444);
			add(500.555);
		}};
		List<Double> inputB = new ArrayList<Double>() {{
			add(999.999);
			add(888.888);
			add(777.777);
			add(666.666);
			add(555.555);
		}};
		List<Double> outputC = new ArrayList<Double>() {{
			add(1.111);
			add(2.222);
			add(3.333);
			add(4.444);
			add(5.555);
		}};
		res.setInput(inputA, inputB);
		res.setOutput(outputC);
		
		String expectedJson =
				  "{"
				+ "    \"output\": {"
				+ "        c: [1.111, 2.222, 3.333, 4.444, 5.555],"
				+ "    },"
				+ "    \"input\": {"
				+ "        a: [100.111, 200.222, 300.333, 400.444, 500.555],"
				+ "        b: [999.999, 888.888, 777.777, 666.666, 555.555]"
				+ "    }"
				+ "}";
		String expected = createExpectedJSONString(expectedJson);
		
		//Execute
		String actual = res.toHttpBody();
		
		//Verify
		assertEquals(expected, actual);
	}
}
