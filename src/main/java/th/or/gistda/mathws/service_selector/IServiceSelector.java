package th.or.gistda.mathws.service_selector;

import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;

public interface IServiceSelector {
	public void process(IMathWsRequest req, IMathWsResponse res);
}
