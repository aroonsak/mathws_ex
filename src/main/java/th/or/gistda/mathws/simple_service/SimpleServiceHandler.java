package th.or.gistda.mathws.simple_service;

import java.util.ArrayList;
import java.util.List;

import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsOperation;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.service_selector.IServiceHandler;

public class SimpleServiceHandler implements IServiceHandler {

	@Override
	public void handle(IMathWsRequest req, IMathWsResponse res) {
		
		ESimpleOperation op = (ESimpleOperation) ESimpleOperation.parse(req.getOperation());
		if (op == null) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid Simple service operation");
			return;
		}
		
		List<Double> inputA = req.getInputA();
		List<Double> inputB = req.getInputB();
		List<Double> outputC = new ArrayList<Double>();
		
		try {
			performOp(op, inputA, inputB, outputC);
		}
		catch (Exception e) {
			res.setService(req.getName(), req.getOperation());
			res.setHttp(HttpConst.STATUS_BAD_REQUEST, e.getMessage());
			return;
		}
		
		res.setService(req.getName(), req.getOperation());
		res.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		res.setInput(inputA, inputB);
		res.setOutput(outputC);
	}
	
	protected void performOp(ESimpleOperation op, List<Double> inputA, List<Double> inputB, List<Double> outputC) throws Exception {
		switch (op) {
		case add:
			simpleAdd(inputA, inputB, outputC);
			break;
		case sub:
			simpleSub(inputA, inputB, outputC);
			break;
		case mul:
			simpleMul(inputA, inputB, outputC);
			break;
		case div:
			simpleDiv(inputA, inputB, outputC);
			break;
		}
	}
	
	protected void simpleAdd(List<Double> inputA, List<Double> inputB, List<Double> outputC) {
		
		for (int idx=0;idx<inputA.size();idx++) {
			outputC.add(inputA.get(idx) + inputB.get(idx));
		}
	}
	
	protected void simpleSub(List<Double> inputA, List<Double> inputB, List<Double> outputC) {
		
		for (int idx=0;idx<inputA.size();idx++) {
			outputC.add(inputA.get(idx) - inputB.get(idx));
		}
	}

	protected void simpleMul(List<Double> inputA, List<Double> inputB, List<Double> outputC) {
	
		for (int idx=0;idx<inputA.size();idx++) {
			outputC.add(inputA.get(idx) * inputB.get(idx));
		}
	}

	protected void simpleDiv(List<Double> inputA, List<Double> inputB, List<Double> outputC) throws Exception {
	
		for (int idx=0;idx<inputA.size();idx++) {
			if (inputB.get(idx) == 0.0)
				throw new Exception("Divide by zero");
			outputC.add(inputA.get(idx) / inputB.get(idx));
		}
	}
}
