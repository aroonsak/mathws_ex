package th.or.gistda.mathws.base;

public class HttpConst {
	
	public static final int STATUS_OK = 200;
	public static final int STATUS_BAD_REQUEST = 400;
	public static final int STATUS_METHOD_NOT_ALLOWED = 405;
	public static final int STATUS_INTERNAL_ERROR = 500;
	
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";

}
