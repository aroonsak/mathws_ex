package th.or.gistda.mathws.base;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RequestValidator {
		
		private static final String servStr = "service";
		private static final String nameStr = "name";
		private static final String opStr = "operation";
		private static final String inputStr = "input";
		private static final String aStr = "a";
		private static final String bStr = "b";

		public static void parseRawRequest(String str, IMathWsRequest mwsReq) throws Exception {
			JSONObject rawRequest = null;
			try {
				rawRequest = new JSONObject(str);
			}
			catch (JSONException ex) {
				throw new Exception("The request is not a valid Math-WS request");
			}
			validateOperation(rawRequest, mwsReq);
			validateInput(rawRequest, mwsReq);
		}
		
		private static boolean validateOperation(JSONObject obj, IMathWsRequest mwsReq) throws Exception {
			JSONObject servObj = null;
			try {
				servObj = obj.getJSONObject(servStr);
			}
			catch (JSONException ex) {
				throw new Exception("Service section is not specified");
			}
			
			String reqName = null;
			try {
				reqName = servObj.getString(nameStr);
			}
			catch (JSONException ex) {
				throw new Exception("Service name is not specified");
			}
			
			String reqOp = null;
			try {
				reqOp = servObj.getString(opStr);
			}
			catch (JSONException ex) {
				throw new Exception("Service operation is not specified");
			}
			
			mwsReq.setName(reqName);
			mwsReq.setOperation(reqOp);
			return true;
		}
		
		private static boolean validateInput(JSONObject obj, IMathWsRequest mwsReq) throws Exception {
			
			JSONObject inputObj = null;
			JSONArray aArr = null, bArr = null;			
			try {
				inputObj = obj.getJSONObject(inputStr);
			} 
			catch (JSONException jsnEx) {
				throw new Exception("Input section is not specified");
			}
			
			try {
				aArr = inputObj.getJSONArray(aStr);
			} 
			catch (JSONException jsnEx) {
				throw new Exception("Input A is not specified");
			}
			
			try {
				bArr = inputObj.getJSONArray(bStr);
			} 
			catch (JSONException jsnEx) {
				throw new Exception("Input B is not specified");
			}
			
			if (aArr.length() != bArr.length()) {
				throw new Exception("Input A and B do not have the same length");
			}
			
			List<Double> a = new ArrayList<Double>();
			List<Double> b = new ArrayList<Double>();
			for (int idx=0;idx<aArr.length();idx++) {
				a.add(aArr.getDouble(idx));
				b.add(bArr.getDouble(idx));
			}
			
			mwsReq.setInputA(a);
			mwsReq.setInputB(b);
			
			return true;
		}
}
