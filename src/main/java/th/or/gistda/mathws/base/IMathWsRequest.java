package th.or.gistda.mathws.base;

import java.util.List;


public interface IMathWsRequest {
	
	public void setName(String name);
	public String getName();
	
	public void setOperation(String op);
	public String getOperation();
	
	public void setInputA(List<Double> a);
	public List<Double> getInputA();
	
	public void setInputB(List<Double> b);
	public List<Double> getInputB();
	
	public void setOutputC(List<Double> c);
	public List<Double> getOutputC();
}
