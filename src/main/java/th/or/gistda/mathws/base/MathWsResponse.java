package th.or.gistda.mathws.base;

import java.util.List;

import org.json.JSONObject;

public class MathWsResponse implements IMathWsResponse {

	private JSONObject master = null;
	
	private JSONObject service = null; //mandatory
	
	private JSONObject http = null;    //mandatory
	private Integer httpStatus = null;
	
	private JSONObject input = null;
	private JSONObject output = null;
	
	public MathWsResponse() {
		master = new JSONObject();
	}
	
	@Override
	public void setService(String name, String operation) {
		
		if (operation == null)
			return;
		
		service = new JSONObject();
		service.put("name", name);
		service.put("operation", operation);
		master.put("service", service);
	}

	@Override
	public void setHttp(Integer status, String description) {
		
		http = new JSONObject();
		master.put("http", http);
		http.put("status", status);
		http.put("description", description);
		
		httpStatus = status;
	}
	
	public Integer getHttpStatus() {
		return httpStatus;
	}
	

	@Override
	public void setInput(List<Double> a, List<Double> b) {
		
		input = new JSONObject();
		master.put("input", input);
		input.put("a", a);
		input.put("b", b);
	}

	@Override
	public void setOutput(List<Double> c) {
		
		output = new JSONObject();
		master.put("output", output);
		output.put("c", c);
	}

	@Override
	public String toHttpBody() {
		
		return master.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		
		MathWsResponse compare = (MathWsResponse) obj;
		
		return this.toHttpBody().equals(compare.toHttpBody());
	}
}
