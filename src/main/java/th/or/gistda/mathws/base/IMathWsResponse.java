package th.or.gistda.mathws.base;

import java.util.List;

public interface IMathWsResponse {
	public void setService(String name, String operation);
	public void setHttp(Integer status, String description);
	public Integer getHttpStatus();
	public void setInput(List<Double> a, List<Double> b);
	public void setOutput(List<Double> c);
	public String toHttpBody();
}
