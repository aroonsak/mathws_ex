import inspect
import traceback

class MathWsTest:

    testKeyword = "__tc__"

    def getTestSuiteName(self):
        return "MathWsTest"

    def runTestSuite(self):
        testSuiteName = self.getTestSuiteName()
        print "===== " + testSuiteName + " (Starting) ====="
        members = inspect.getmembers(self)
        resultList = []

        totalCnt = 0
        passedCnt = 0
        failedCnt = 0
        for member in members:
            kOffset = member[0].find(MathWsTest.testKeyword)
            nameOffset = kOffset + len(MathWsTest.testKeyword)
            if (kOffset >= 0):
                tcName = member[0][nameOffset:]
                print "\t\033[95mRun " + tcName + "\033[0m"
                try:
                    member[1]()
                    resultList.append((tcName, "\033[1m\033[92mPASSED\033[0m", True))
                    totalCnt += 1
                    passedCnt += 1
                except Exception as e:
                    print "\033[1m\033[91m[Exception]: " + e.message + "\033[0m"
                    if not (traceback.print_exc() == None):
                        print "\033[1m\033[91m[Exception]: " + traceback.print_exc() + "\033[0m"
                    resultList.append((tcName, "\033[1m\033[91mFAILED\033[0m", False))
                    totalCnt += 1
                    failedCnt += 1
        
        print "===== " + testSuiteName + " (Result) ====="
        for result in resultList:
            print "\t" + result[0] + "\t:\t" + result[1]
        print "\n\t*** Total:" + str(totalCnt) + "\033[1m\033[92m Passed:" + str(passedCnt),
        if (failedCnt > 0):
            print "\033[1m\033[91m Failed:" + str(failedCnt),
        print  "\033[0m***"

        return resultList