import json
import requests

mathWsEndpoint = "http://localhost:8080/mathapi"

class MathWsRequest:
    def __init__(self):
        self.request = {}

    def setService(self, serviceName, serviceOperation):
        service = {}
        service["name"] = serviceName
        service["operation"] = serviceOperation
        self.request["service"] = service

    def getService():
        return self.request["service"]

    def setInput(self, inputA, inputB):
        inputEntry = {}
        inputEntry["a"] = inputA
        inputEntry["b"] = inputB
        self.request["input"] = inputEntry

    def getRequestBody(self):
        return json.dumps(self.request)

class MathWsResponse:
    def __init__(self, responseText=None):
        self.response = None

        if (responseText == None):
            self.response = {}
        else:
            self.response = json.loads(responseText)

    def getResponse(self):
        return self.response

    def getResponseBody(self):
        return json.dumps(self.response)

    def setHttp(self, status, description):
        httpBlock = {
            "status": status,
            "description": description
        }
        self.response["http"] = httpBlock

    def setService(self, serviceName, serviceOperation):
        serviceBlock = {
            "name": serviceName,
            "operation": serviceOperation
        }
        self.response["service"] = serviceBlock

    def setInput(self, inputA, inputB):
        inputBlock = {
            "inputA": inputA,
            "inputB": inputB
        }
        self.response["input"] = inputBlock

    def setOutput(self, output):
        outputBlock = {
            "c": output
        }
        self.response["output"] = outputBlock

    def compareHttp(self, http, anotherHttp):
        if (http["status"] != anotherHttp["status"]):
            return False
        if (http["description"] != anotherHttp["description"]):
            return False
        return True

    def compareService(self, service, anotherService):
        if (service["name"] != anotherService["name"]):
            return False
        if (service["operation"] != anotherService["operation"]):
            return False
        return True

    def compareListExact(self, list_1, list_2):
        if not (len(list_1) == len(list_2)):
            return False

        for i in range(0, len(list_1)):
            if not (list_1[i] == list_2[i]):
                return False

        return True


    def compareInput(self, input, anotherInput):
        result = True

        if ("a" in input):
            result = result and self.compareListExact(input["a"], anotherInput["a"])
        if ("b" in input):
            result = result and self.compareListExact(input["b"], anotherInput["b"])

        return result

    def compareListWithTolerance(self, list_1, list_2):
        if (not (len(list_1) == len(list_2))):
            return False

        for i in range(0, len(list_1)):
            if (list_1[i] == 0):
                continue
            diff = abs(list_1[i] - list_2[i])
            if (abs(diff/list_1[i]) > 1e-15):
                return False

        return True

    def compareOutput(self, output, anotherOutput):
        if (not "c" in anotherOutput):
            return False
        return self.compareListWithTolerance(output["c"], anotherOutput["c"])

    def equals(self, another):

        anotherRes = another.getResponse()

        result = True

        if "http" in self.response:
            result = result and self.compareHttp(self.response["http"], anotherRes["http"])
            if (not result): print("FAILED HTTP")
        if "service" in self.response:
            result = result and self.compareService(self.response["service"], anotherRes["service"])
            if (not result): print("FAILED SERVICE")
        if "input" in self.response:
            result = result and self.compareInput(self.response["input"], anotherRes["input"])
            if (not result): print("FAILED INPUT")
        if "output" in self.response:
            result = result and self.compareOutput(self.response["output"], anotherRes["output"])
            if (not result): print("FAILED OUTPUT")
        return result


def testPostWithParams(serviceName, serviceOperation, inputA, inputB):
    req = MathWsRequest()
    req.setService(serviceName, serviceOperation)
    req.setInput(inputA, inputB)

    rawRes = requests.post(mathWsEndpoint, data = req.getRequestBody())
    res = MathWsResponse(rawRes.text)

    return res

def testGet():
    rawRes = requests.get(mathWsEndpoint)
    res = MathWsResponse(rawRes.text)
    return res



